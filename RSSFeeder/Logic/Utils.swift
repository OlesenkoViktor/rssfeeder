//
//  Utils.swift
//  RSSFeeder
//
//  Created by Viktor Olesenko on 20.09.16.
//  Copyright © 2016 Viktor Olesenko. All rights reserved.
//

import UIKit

class Utils: NSObject {
    
    // MARK: - DateFormatter
    
    static let itemCreationDateFromatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd HH:mm"
        return dateFormatter
    }()
    
}
