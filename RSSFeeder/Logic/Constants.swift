//
//  Constants.swift
//  RSSFeeder
//
//  Created by Viktor Olesenko on 20.09.16.
//  Copyright © 2016 Viktor Olesenko. All rights reserved.
//

import UIKit

struct RSSKey {
    static let Item        = "item"
    static let Url         = "link"
    static let Title       = "title"
    static let Description = "description"
    static let Attached    = "enclosure"
    static let ImageTR     = "tr:image"
    static let PhotoUrl    = "url"
    static let CreatedDate = "pubDate"
}
