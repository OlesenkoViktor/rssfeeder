//
//  RSSManager.swift
//  RSSFeeder
//
//  Created by Viktor Olesenko on 20.09.16.
//  Copyright © 2016 Viktor Olesenko. All rights reserved.
//

import UIKit

typealias CompletitionBlock = (_ title: String?, _ items: [RSSItem]?, _ error: Error?) -> Void

class RSSManager: NSObject, XMLParserDelegate {
    
    // MARK: - Propertys
    
    private var xmlParser: XMLParser!
    private var completition: CompletitionBlock?
    private var requestTimeoutTimer: Timer?
    
    private var title: String?
    private var items = [RSSItem]()
    
    private var currentElement: String!
    private var currentItem: RSSItem?
    
    private static var rssItemDateFromatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE, dd MMM yyyy HH:mm:ss ZZZZ"
        return dateFormatter
    }()
    
    // MARK: - Parse Flow
    
    func loadItems(rssUrl: String, completition: @escaping CompletitionBlock) {
        
        requestTimeoutTimer = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(requestTimeout), userInfo: nil, repeats: false)
        
        xmlParser = XMLParser(contentsOf: URL(string: rssUrl)!)
        self.completition = completition
        xmlParser.delegate = self
        xmlParser.parse()
    }
    
    @objc private func requestTimeout() {
        self.parser(self.xmlParser, parseErrorOccurred: NSError("Request timeout"))
    }
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        currentElement = elementName
        
        if elementName == RSSKey.Item {
            currentItem = RSSItem()
        }
        
        if elementName == RSSKey.Attached,
            let photoUrl = attributeDict[RSSKey.PhotoUrl] {
            currentItem?.photoUrl = photoUrl
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        
        if currentElement != nil &&
            currentElement == RSSKey.Title &&
            title == nil {
            title = string
            return
        }
        
        guard !string.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else { return }
        
        switch currentElement {
        case RSSKey.Url:         currentItem?.url      = string
        case RSSKey.Title:       currentItem?.title    = string
        case RSSKey.ImageTR:     currentItem?.photoUrl = string
        case RSSKey.Description: currentItem?.text     = string
        case RSSKey.CreatedDate: currentItem?.date     = RSSManager.rssItemDateFromatter.date(from: string)!
        default: break
        }
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if elementName == RSSKey.Item &&
            currentItem != nil {
            items.append(currentItem!)
            currentItem = nil
        }
        
        currentElement = nil
    }
    
    func parserDidStartDocument(_ parser: XMLParser) {
        requestTimeoutTimer?.invalidate()
        requestTimeoutTimer = nil
    }
    
    func parserDidEndDocument(_ parser: XMLParser) {
        completition?(title ?? "", items, nil)
    }
    
    func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error) {
        completition?(nil, nil, parseError)
        requestTimeoutTimer?.invalidate()
        requestTimeoutTimer = nil
    }
}
