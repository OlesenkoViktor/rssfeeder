//
//  VideoTabTableViewCell.swift
//  RSSFeeder
//
//  Created by Viktor Olesenko on 20.09.16.
//  Copyright © 2016 Viktor Olesenko. All rights reserved.
//

import UIKit

class VideoTabTableViewCell: UITableViewCell {

    @IBOutlet var titleLabel:       UILabel!
    @IBOutlet var descriptionLabel: UILabel!
}
