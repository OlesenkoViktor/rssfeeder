//
//  VideosTabViewController.swift
//  RSSFeeder
//
//  Created by Viktor Olesenko on 20.09.16.
//  Copyright © 2016 Viktor Olesenko. All rights reserved.
//

import UIKit

class VideosTabViewController: RootTabViewController {
    
    // MARK: - Init
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initTableView()

        self.rssUrl = "http://www.techrepublic.com/rssfeeds/videos/"
    }
    
    private func initTableView() {
        rssItemsTableView.estimatedRowHeight = 47
        rssItemsTableView.rowHeight          = UITableViewAutomaticDimension
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - UITableView
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item = rssItems[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "videoCellIdentifier") as! VideoTabTableViewCell
        cell.titleLabel.text       = item.title
        cell.descriptionLabel.text = item.text
        
        return cell
    }
}
