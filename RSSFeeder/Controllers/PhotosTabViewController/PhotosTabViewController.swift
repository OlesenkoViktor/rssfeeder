//
//  PhotosTabViewController.swift
//  RSSFeeder
//
//  Created by Viktor Olesenko on 20.09.16.
//  Copyright © 2016 Viktor Olesenko. All rights reserved.
//

import UIKit

class PhotosTabViewController: RootTabViewController {
    
    // MARK: - Init
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initTableView()
        
        self.rssUrl = "http://www.techrepublic.com/rssfeeds/image-gallery/"
    }
    
    private func initTableView() {
        rssItemsTableView.rowHeight = 80
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - UITableView
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item = rssItems[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "photoCellIdentifier") as! PhotoTabTableViewCell
        cell.titleLabel.text = item.title
        
        cell.dateLabel.text = nil
        if let itemCreationDate = item.date {
            cell.dateLabel.text  = Utils.itemCreationDateFromatter.string(from: itemCreationDate)
        }
        
        cell.thumbImageView.image = UIImage(named: "image_placeholder")
        if let articlePhotoUrl = item.photoUrl {
            cell.thumbImageView.loadImage(fromURL: articlePhotoUrl)
        }
        
        return cell
    }
}
