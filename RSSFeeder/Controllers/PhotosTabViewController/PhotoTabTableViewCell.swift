//
//  PhotoTabTableViewCell.swift
//  RSSFeeder
//
//  Created by Viktor Olesenko on 20.09.16.
//  Copyright © 2016 Viktor Olesenko. All rights reserved.
//

import UIKit

class PhotoTabTableViewCell: UITableViewCell {
    
    @IBOutlet var thumbImageView: UIImageView!
    @IBOutlet var titleLabel:     UILabel!
    @IBOutlet var dateLabel:      UILabel!
}
