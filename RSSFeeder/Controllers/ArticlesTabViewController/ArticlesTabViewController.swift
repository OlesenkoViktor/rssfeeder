//
//  ArticlesTabViewController.swift
//  RSSFeeder
//
//  Created by Viktor Olesenko on 20.09.16.
//  Copyright © 2016 Viktor Olesenko. All rights reserved.
//

import UIKit

class ArticlesTabViewController: RootTabViewController {
    
    // MARK: - Init
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initTableView()
        
        self.rssUrl = "http://www.techrepublic.com/rssfeeds/articles/latest/"
    }
    
    private func initTableView() {
        rssItemsTableView.estimatedRowHeight = 56
        rssItemsTableView.rowHeight          = UITableViewAutomaticDimension
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - UITableView
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item = rssItems[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "articleCellIdentifier") as! ArticleTabTableViewCell
        cell.titleLabel.text = item.title
        
        if let itemCreationDate = item.date {
            cell.dateLabel.text  = Utils.itemCreationDateFromatter.string(from: itemCreationDate)
        }
        
        return cell
    }
}
