//
//  RootTabViewController.swift
//  RSSFeeder
//
//  Created by Viktor Olesenko on 20.09.16.
//  Copyright © 2016 Viktor Olesenko. All rights reserved.
//

import UIKit

class RootTabViewController: UIViewController {
    
    @IBOutlet internal var loader: UIActivityIndicatorView?
    @IBOutlet internal var rssItemsTableView: UITableView!
    
    var rssUrl: String? {
        didSet {
            reloadData()
        }
    }
    
    var rssItems: [RSSItem] = []
    
    // MARK: - Init
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initTableView()
        
        self.navigationController?.navigationBar.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onNavBar)))
    }
    
    private func initTableView() {
        rssItemsTableView.dataSource = self
        rssItemsTableView.delegate   = self
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(reloadData(refreshControl:)), for: .valueChanged)
        rssItemsTableView.addSubview(refreshControl)
    }
    
    @objc private func reloadData(refreshControl: UIRefreshControl? = nil) {
        rssItemsTableView?.isHidden = true
        loader?.isHidden = false
        
        rssItems.removeAll()
        RSSManager().loadItems(rssUrl: rssUrl!) { (title, items, error) in
            self.loader?.isHidden = true
            
            if error == nil {
                self.rssItemsTableView?.isHidden = false
                self.title = title
                self.rssItems = items!
                
                UIView.animate(withDuration: 0.3, animations: {
                    refreshControl?.endRefreshing()
                    }, completion: { (completed) in
                        self.rssItemsTableView.reloadData()
                })
            } else {
                self.present(Alert.alert(text: error!.localizedDescription), animated: true, completion: nil)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if rssItems.isEmpty { reloadData() }
    }
    
    // MARK: - Actions
    
    @objc private func onNavBar() {
        
        let alertController = UIAlertController(title: "Enter RSS url", message: "Please, provide new RSS url", preferredStyle: .alert)
        
        alertController.addTextField(configurationHandler: { (textField) in
            textField.placeholder   = "Enter RSS url"
            textField.keyboardType  = .URL
        })
        
        let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
            let url = alertController.textFields!.first!.text
            
            self.rssUrl = url
        }
        alertController.addAction(okAction)
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
}

// MARK: - UITableViewDataSource

extension RootTabViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rssItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // override in subclass
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
