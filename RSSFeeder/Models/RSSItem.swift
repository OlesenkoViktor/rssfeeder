//
//  RSSItem.swift
//  RSSFeeder
//
//  Created by Viktor Olesenko on 20.09.16.
//  Copyright © 2016 Viktor Olesenko. All rights reserved.
//

import UIKit

struct RSSItem {
    var url:      String?
    var title:    String?
    var text:     String?
    var photoUrl: String?
    var date:     Date?
}
