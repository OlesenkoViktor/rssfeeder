//
//  UIImageView+Url.swift
//  RSSFeeder
//
//  Created by Viktor Olesenko on 20.09.16.
//  Copyright © 2016 Viktor Olesenko. All rights reserved.
//

import UIKit

extension UIImageView {
    
    func loadImage(fromURL url: String) {
        
        DispatchQueue.global().async {
            do {
                let imageData = try Data(contentsOf: URL(string: url)!)
                let loadedImage = UIImage(data: imageData)
                DispatchQueue.main.async {
                    self.image = loadedImage
                }
            } catch {
                print(error.localizedDescription)
            }
        }
    }    
}
