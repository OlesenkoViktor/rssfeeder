//
//  NSError+Text.swift
//  RSSFeeder
//
//  Created by Viktor Olesenko on 20.09.16.
//  Copyright © 2016 Viktor Olesenko. All rights reserved.
//

import UIKit

extension NSError {
    
    convenience init(_ text: String) {
        self.init(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey : text])
    }    
}
